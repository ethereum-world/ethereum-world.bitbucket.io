import React from "react";
import "./App.css";
import Particles from 'react-particles-js';
import Tree from 'react-d3-tree';
import { appData } from './data.js';
import { getForeignRequest } from './utils/request';


export default class App extends React.Component {

    constructor(props) {
        super(props);

        this.generateLevel = this.generateLevel.bind(this);
        this.changeLevelStatus = this.changeLevelStatus.bind(this);

        this.state = {
            logged: false,
            apps: appData,
            gas: null,
            price: null,
            displayDefiOnly: false,
            openLevels: [],
            openAll: false,
        };
    }

    componentDidMount() {
        getForeignRequest.call(this, "https://www.etherchain.org/api/gasPriceOracle", data => {
            this.setState({
                gas: data,
            });
        }, response => {
            console.log(response);
        }, error => {
            console.log(error);
        });

        getForeignRequest.call(this, "https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=usd,eur&include_market_cap=true&include_24hr_vol=true&include_24hr_change=true", data => {
            this.setState({
                price: data,
            });
        }, response => {
            console.log(response);
        }, error => {
            console.log(error);
        });
    }

    generateLevel(level) {
        if (level.length > 0) {
            let shouldContinue = this.state.openAll;

            if (!shouldContinue) {
                this.state.openLevels.map((l) => {
                    if (JSON.stringify(level) === JSON.stringify(l)) {
                        shouldContinue = true
                    }
                });
            }

            if (!shouldContinue) {
                return [];
            }
        }

        let categories = [];
        let apps = [];

        this.state.apps.map((a) => {
            a.taxonomy.map((t) => {
                if (JSON.stringify(level) === JSON.stringify(t.slice(0, level.length))) {
                    if (level.length < t.length) {
                        categories.push(t[level.length]);
                    } else {
                        if (!this.state.displayDefiOnly || a.isDefi) {
                            apps.push(a);
                        }
                    }
                }
            });
        });

        categories = [...new Set(categories)];
        categories.sort((a, b) => a > b ? 1 : -1);
        categories = categories.map((c) => { return { name: c } });

        apps.sort((a, b) => a.name > b.name ? 1 : -1);

        let blocks = categories.concat(apps);

        return <div>
            {blocks.map((b) => {
                return this.renderBlock(b, level);
            })}
        </div>
    }

    renderBlock(info, parentLevel) {
        if (info.image || info.url) {
            console.log(info.url);
            return <a href={info.url} target="_blank">
                <div
                    className="childBlock">
                    <img src={info.image}/> 
                    <div className="childBlock-text">
                        {info.name}
                    </div>
                </div>
            </a>
        } else {
            return <div className="relationshipBlock">
                <div
                    className="parentBlock"
                    onClick={() => this.changeLevelStatus(parentLevel.concat([info.name]))}
                >
                   <img src="/img/logo.png"/> 
                   <div className="parentBlock-text">{info.name}</div>
                </div>

                {this.generateLevel(parentLevel.concat([info.name]))}
            </div>
        }
    }

    changeLevelStatus(level) {
        let isInList = false;

        this.state.openLevels.map((l) => {
            if (JSON.stringify(level) === JSON.stringify(l)) {
                isInList = true;
            }
        });

        this.setState({
            openLevels: isInList
                ? this.state.openLevels.filter((l) => JSON.stringify(level) !== JSON.stringify(l))
                : this.state.openLevels.concat([level])
        });
    }

    getColors(url) {
        const { data, loading, error } = usePalette(url);
        console.log(data, loading, error);
    }

    render() {
        return (
            <div id="App">
                <Particles 
                    params={{
                        "particles": {
                            "number": {
                                "value": 50
                            },
                            "size": {
                                "value": 9
                            },
                            "color": {
                                "value": ["#009fe3", "#e40613"]
                            },
                            "shape": {
                              "type": "images",
                              "images": [
                                {
                                  "src": "/img/logo.png",
                                  "width": 2500000,
                                  "height": 2500000,
                                },
                              ]
                            },
                            "move": {
                              "enable": true,
                              "speed": 0.3,
                            },
                            "opacity": {
                                value: 0.2,
                                anim: {
                                    enable: false
                                }
                            },
                            "line_linked": {
                              "enable": true,
                              "distance": 150,
                              "color": {
                                "value": "#ecf0f1"
                              },
                              "opacity": 0.05,
                              "width": 1
                            },
                        },
                    }}
                />

                <div className={"table"}>
                    {this.generateLevel([])}
                </div>

                <div className="top-right">
                    <h1>Price</h1>

                    {this.state.price !== null ?
                        <div className="top-right-block">
                            <div className="top-right-value">{this.state.price.ethereum.usd}</div>
                            <div className="top-right-desc">USD</div>
                        </div>
                    : ""}
                    {this.state.price !== null ?
                        <div className="top-right-block">
                            <div className="top-right-value">{this.state.price.ethereum.eur}</div>
                            <div className="top-right-desc">EUR</div>
                        </div>
                    : ""}

                    <h1>Gas</h1>

                    {this.state.gas !== null ?
                        Object.keys(this.state.gas)
                            .filter((k) => k.includes("BaseFee"))
                            .map(g => {
                            return (
                                <div className="top-right-block">
                                    <div className="top-right-value">{this.state.gas[g]}</div>
                                    <div className="top-right-desc">{g.replace(/([A-Z])/g, " $1")}</div>
                                </div>
                            );
                        })
                    : ""}

                    <div className="top-right-block top-right-block-bottom">
                        <input 
                            type="checkbox" 
                            defaultChecked={this.state.openAll} 
                            onChange={() => this.setState({ openAll: !this.state.openAll })} 
                        />
                        <div className="top-right-desc">Open all</div>
                        <br/>
                        <input 
                            type="checkbox" 
                            defaultChecked={this.state.displayDefiOnly} 
                            onChange={() => this.setState({ displayDefiOnly: !this.state.displayDefiOnly })} 
                        />
                        <div className="top-right-desc">DeFi only</div>
                    </div>
                </div>
            </div>
        );
    }
}