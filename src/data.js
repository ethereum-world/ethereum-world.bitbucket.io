export const appData = [
    {
        "name": "Paladin",
        "url": "https://www.paladin.vote/",
        "image": "https://www.paladin.vote/src/media/logo/icon_transparent.png",
        "isDefi": true,
        "taxonomy": [
            ["DAO", "Vote Delegation"]
        ] 
    },
    {
        "name": "ElasticDAO",
        "url": "https://elasticdao.org/",
        "image": "https://elasticdao.org/",
        "isDefi": true,
        "taxonomy": [
            ["DAO", "Tool"]
        ] 
    },
    {
        "name": "Snapshot",
        "url": "https://snapshot.org/#/",
        "image": "https://snapshot.org/favicon.png",
        "isDefi": true,
        "taxonomy": [
            ["DAO", "Tool"]
        ] 
    },
    {
        "name": "Gnosis Safe",
        "url": "https://gnosis-safe.io/",
        "image": "https://gnosis-safe.io/favicon/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Wallet", "Multisig"]
        ] 
    },
    {
        "name": "Jarvis Network",
        "url": "https://jarvis.network/",
        "image": "https://jarvis.network/assets/images/jarvis-logo-icon.png",
        "isDefi": true,
        "taxonomy": [
            ["Synthetics"]
        ] 
    },
    {
        "name": "Mirror Protocol",
        "url": "https://mirror.finance/",
        "image": "https://mirror.finance/assets/img/favicon.svg",
        "isDefi": true,
        "taxonomy": [
            ["Synthetics"]
        ] 
    },
    {
        "name": "FTX",
        "url": "https://ftx.com/",
        "image": "https://ftx.com/favicon.ico?v=2",
        "isDefi": false,
        "taxonomy": [
            ["Exchange", "Derivatives"]
        ] 
    },
    {
        "name": "Opyn",
        "url": "https://opyn.co/#/",
        "image": "https://opyn.co/favicon.ico",
        "isDefi": false,
        "taxonomy": [
            ["Exchange", "Derivatives"]
        ] 
    },
	{
        "name": "MakerDAO DAI",
        "url": "https://makerdao.com/en/",
        "image": "https://makerdao.com/android-icon-192x192.png",
        "isDefi": true,
        "taxonomy": [
            ["Coin", "Stable Coin", "USD"]
        ] 
    },
    {
        "name": "$DPI",
        "url": "https://defipulse.com/",
        "image": "https://defipulse.com/static/Images/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Coin", "Index"]
        ] 
    },
    {
        "name": "$PLAY",
        "url": "https://www.piedao.org/#/pies",
        "image": "https://raw.githubusercontent.com/pie-dao/brand/master/PieDAO%20Logo/PieDAO%20Icon%20Clean.png",
        "isDefi": true,
        "taxonomy": [
            ["Coin", "Index"]
        ] 
    },
    {
        "name": "$DEFI+L",
        "url": "https://www.piedao.org/#/pies",
        "image": "https://raw.githubusercontent.com/pie-dao/brand/master/PieDAO%20Logo/PieDAO%20Icon%20Clean.png",
        "isDefi": true,
        "taxonomy": [
            ["Coin", "Index"]
        ] 
    },
    {
        "name": "$YPIE",
        "url": "https://www.piedao.org/#/pies",
        "image": "https://raw.githubusercontent.com/pie-dao/brand/master/PieDAO%20Logo/PieDAO%20Icon%20Clean.png",
        "isDefi": true,
        "taxonomy": [
            ["Coin", "Index"]
        ] 
    },
    {
        "name": "$USD++",
        "url": "https://www.piedao.org/#/pies",
        "image": "https://raw.githubusercontent.com/pie-dao/brand/master/PieDAO%20Logo/PieDAO%20Icon%20Clean.png",
        "isDefi": true,
        "taxonomy": [
            ["Coin", "Index"]
        ] 
    },
    {
        "name": "Beefy Finance",
        "url": "https://www.beefy.finance/",
        "image": "https://www.beefy.finance/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Investment Strategy", "Yield Aggregator"]
        ] 
    },
    {
        "name": "Adamant Finance",
        "url": "https://adamant.finance/",
        "image": "https://adamant.finance/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Investment Strategy", "Yield Aggregator"]
        ] 
    },
    {
        "name": "Convex Finance",
        "url": "https://www.convexfinance.com/",
        "image": "https://www.convexfinance.com/static/favicons/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Investment Strategy", "Performance Optimizer"]
        ] 
    },
    {
        "name": "StakeDAO",
        "url": "https://stakedao.org/",
        "image": "https://stakedao.org/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Investment Strategy", "Yield Aggregator"],
            ["Wallet", "Dashboard", "Portfolio"]
        ] 
    },
    {
        "name": "PieDAO BCP",
        "url": "https://www.piedao.org/#/pies",
        "image": "https://raw.githubusercontent.com/pie-dao/brand/master/PieDAO%20Logo/PieDAO%20Icon%20Clean.png",
        "isDefi": true,
        "taxonomy": [
            ["Investment Strategy", "Fund"]
        ] 
    },
    {
        "name": "PieDAO DEFI++",
        "url": "https://www.piedao.org/#/pies",
        "image": "https://raw.githubusercontent.com/pie-dao/brand/master/PieDAO%20Logo/PieDAO%20Icon%20Clean.png",
        "isDefi": true,
        "taxonomy": [
            ["Investment Strategy", "Fund"]
        ] 
    },
    {
        "name": "PieDAO DEFI+S",
        "url": "https://www.piedao.org/#/pies",
        "image": "https://raw.githubusercontent.com/pie-dao/brand/master/PieDAO%20Logo/PieDAO%20Icon%20Clean.png",
        "isDefi": true,
        "taxonomy": [
            ["Investment Strategy", "Fund"]
        ] 
    },
    {
        "name": "PieDAO BTC++",
        "url": "https://www.piedao.org/#/pies",
        "image": "https://raw.githubusercontent.com/pie-dao/brand/master/PieDAO%20Logo/PieDAO%20Icon%20Clean.png",
        "isDefi": true,
        "taxonomy": [
            ["Investment Strategy", "Fund"]
        ] 
    },
    {
        "name": "Circle USDC",
        "url": "https://www.circle.com/en/usdc",
        "image": "https://www.circle.com/hubfs/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Coin", "Stable Coin", "USD"]
        ] 
    },
    {
        "name": "Tether USDT",
        "url": "https://tether.to/",
        "image": "https://tether.to/wp-content/uploads/2018/09/TetherFavicon_diamon.png",
        "isDefi": false,
        "taxonomy": [
            ["Coin", "Stable Coin", "USD"]
        ] 
    },
    {
        "name": "Ampleforth",
        "url": "https://www.ampleforth.org/",
        "image": "https://assets.fragments.org/images/favicon/android-icon-192x192.png",
        "isDefi": true,
        "taxonomy": [
            ["Coin", "Rebasing Coin"]
        ] 
    },
    {
        "name": "Monolith",
        "url": "https://monolith.xyz/",
        "image": "https://monolith.xyz/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Payment", "Card"]
        ] 
    },
    {
        "name": "Mooni",
        "url": "https://mooni.tech/",
        "image": "https://mooni.tech/favicon.ico",
        "isDefi": false,
        "taxonomy": [
            ["Exchange", "Off-Ramp"]
        ] 
    },
    {
        "name": "Loopring",
        "url": "https://loopring.org/#/",
        "image": "https://loopring.org/images/favicon/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Scaling Solution", "Layer 2"]
        ] 
    },
    {
        "name": "Arbitrum",
        "url": "https://arbitrum.io/",
        "image": "https://arbitrum.io/wp-content/uploads/2021/01/cropped-Arbitrum_Symbol-Full-color-White-background-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Scaling Solution", "Layer 2"]
        ] 
    },
    {
        "name": "Polygon",
        "url": "https://polygon.technology/",
        "image": "https://polygon.technology/wp-content/uploads/2021/02/cropped-polygon-ico-32x32.png",
        "isDefi": false,
        "taxonomy": [
            ["Scaling Solution", "Side Chain"]
        ] 
    },
    {
        "name": "DeversiFi",
        "url": "https://www.deversifi.com/",
        "image": "https://www.deversifi.com/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Exchange", "DEX"]
        ] 
    },
    {
        "name": "DeXe network",
        "url": "https://dexe.network/",
        "image": "https://dexe.network/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Investment Strategy", "Copy Trading"]
        ] 
    },
    {
        "name": "Dharma",
        "url": "https://www.dharma.io/",
        "image": "https://www.dharma.io/favicon.ico",
        "isDefi": false,
        "taxonomy": [
            ["Exchange", "On-Ramp"]
        ] 
    },
    {
        "name": "Crypto.com",
        "url": "https://crypto.com/en/index.html",
        "image": "https://crypto.com/favicon-32x32.png?v=0f6f06777a5d4bc338bfeca412628e1c",
        "isDefi": false,
        "taxonomy": [
            ["Payment", "Card"],
            ["Credit", "Loan"],
            ["Credit", "Borrow"],
            ["Exchange", "CEX"]
        ] 
    },
    {
        "name": "Alchemix",
        "url": "https://alchemix.fi/",
        "image": "https://alchemix.fi/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Credit", "Borrow"],
        ] 
    },
    {
        "name": "Binance",
        "url": "https://www.binance.com/en",
        "image": "https://bin.bnbstatic.com/static/images/common/favicon.ico",
        "isDefi": false,
        "taxonomy": [
            ["Payment", "Card"],
            ["Credit", "Loan"],
            ["Exchange", "CEX"]
        ] 
    },
    {
        "name": "Zapper",
        "url": "https://zapper.fi/dashboard",
        "image": "https://zapper.fi/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Wallet", "Dashboard", "Portfolio"] 
        ]
    },
    {
        "name": "Celsius Network",
        "url": "https://celsius.network/",
        "image": "https://celsius.network/favicon.ico",
        "isDefi": false,
        "taxonomy": [
            ["Credit", "Loan"],
            ["Credit", "Borrow"]
        ]
    },
    {
        "name": "Liquity",
        "url": "https://www.liquity.org/",
        "image": "https://uploads-ssl.webflow.com/5fd883457ba5da4c3822b02c/6009423cac0ae72be991df28_favicon.png",
        "isDefi": true,
        "taxonomy": [
            ["Credit", "Borrow"]
        ]
    },
    {
        "name": "Curve",
        "url": "https://www.curve.fi/",
        "image": "https://www.curve.fi/favicon-32x32.svg",
        "isDefi": true,
        "taxonomy": [
            ["Exchange", "DEX"]
        ]
    },
    {
        "name": "Swerve",
        "url": "https://www.swerve.fi/",
        "image": "https://swerve.fi/img/favicon/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Exchange", "DEX"]
        ]
    },
    {
        "name": "1inch",
        "url": "https://1inch.exchange/",
        "image": "https://1inch.exchange/assets/favicon/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Exchange", "DEX Aggregator"]
        ]
    },
    {
        "name": "Matcha",
        "url": "https://matcha.xyz/",
        "image": "https://matcha.xyz/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Exchange", "DEX Aggregator"]
        ]
    },
    {
        "name": "ParaSwap",
        "url": "https://paraswap.io/",
        "image": "https://paraswap.io/public/images/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Exchange", "DEX Aggregator"]
        ]
    },
    {
        "name": "Uniswap",
        "url": "https://app.uniswap.org/",
        "image": "https://app.uniswap.org/favicon.png",
        "isDefi": true,
        "taxonomy": [
            ["Exchange", "DEX"]
        ]
    },
    {
        "name": "Sushiswap",
        "url": "https://www.sushiswap.fi/",
        "image": "https://www.sushiswap.fi/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Exchange", "DEX"]
        ]
    },
    {
        "name": "Kleros",
        "url": "https://kleros.io/",
        "image": "https://kleros.io/favicon.svg?v=bcd0ce169f2a262c60ec2cbca43166ee",
        "isDefi": true,
        "taxonomy": [
            ["Justice"]
        ]
    },
    {
        "name": "Chainlink",
        "url": "https://chain.link/",
        "image": "https://assets-global.website-files.com/5f6b7190899f41fb70882d08/5fa97474da20bc75f63bfdc4_Chainlink-favicon-32.png",
        "isDefi": true,
        "taxonomy": [
            ["Oracle"]
        ]
    },
    {
        "name": "UMA",
        "url": "https://umaproject.org/",
        "image": "https://umaproject.org/assets/images/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Oracle"]
        ]
    },
    {
        "name": "Metamask",
        "url": "https://metamask.io/",
        "image": "https://metamask.io/images/favicon.png",
        "isDefi": true,
        "taxonomy": [
            ["Wallet", "Soft Wallet"]
        ]
    },
    {
        "name": "Ledger",
        "url": "https://www.ledger.com/",
        "image": "https://www.ledger.com/wp-content/uploads/2017/08/Ledger_favicon152x152-150x150.png",
        "isDefi": true,
        "taxonomy": [
            ["Wallet", "Hard Wallet"]
        ]
    },
    {
        "name": "NGrave",
        "url": "https://www.ngrave.io/",
        "image": "https://v.fastcdn.co/u/810ecb75/52721251-0-NGRAVE-Beeldmerk.png",
        "isDefi": true,
        "taxonomy": [
            ["Wallet", "Hard Wallet"]
        ]
    },
    {
        "name": "Argent",
        "url": "https://www.argent.xyz/",
        "image": "https://www.argent.xyz/favicon-32x32.png?v=492fdf43c2194271dfad036e41a24c51",
        "isDefi": true,
        "taxonomy": [
            ["Wallet", "Soft Wallet"]
        ]
    },
    {
        "name": "XDEFI Wallet",
        "url": "https://www.xdefi.io/",
        "image": "https://www.xdefi.io/wp-content/uploads/2021/04/cropped-favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Wallet", "Soft Wallet"]
        ]
    },
    {
        "name": "DeFi Pulse",
        "url": "https://defipulse.com/",
        "image": "https://defipulse.com/static/Images/favicon.ico",
        "isDefi": false,
        "taxonomy": [
            ["Analytics"]
        ]
    },
    {
        "name": "Crypto Fees",
        "url": "https://cryptofees.info/",
        "image": "https://cryptofees.info/favicon.png",
        "isDefi": false,
        "taxonomy": [
            ["Analytics"]
        ]
    },
    {
        "name": "CryptoStats",
        "url": "https://cryptostats.community/",
        "image": "https://cryptostats.community/favicon.png",
        "isDefi": false,
        "taxonomy": [
            ["Analytics"],
            ["Developer", "Data"]
        ]
    },
    {
        "name": "Dune Analytics",
        "url": "https://dune.xyz/",
        "image": "https://dune.xyz/assets/glyph-128w.png",
        "isDefi": false,
        "taxonomy": [
            ["Analytics"],
            ["Developer", "Data"]
        ]
    },
    {
        "name": "TokenBrice",
        "url": "https://tokenbrice.xyz/",
        "image": "https://tokenbrice.xyz/favicon.ico",
        "isDefi": false,
        "taxonomy": [
            ["Media", "Blog/News"]
        ]
    },
    {
        "name": "Bankless",
        "url": "https://banklesshq.com/",
        "image": "https://bucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com/public/images/54ddfd59-2bad-4546-b7f7-ea5ef6ba7829/favicon.ico",
        "isDefi": false,
        "taxonomy": [
            ["Media", "Blog/News"]
        ]
    },
    {
        "name": "rekt",
        "url": "https://rekt.news/",
        "image": "https://raw.githubusercontent.com/RektHQ/Assets/main/images/favicon.png",
        "isDefi": false,
        "taxonomy": [
            ["Media", "Blog/News"]
        ]
    },
    {
        "name": "DeBank",
        "url": "https://debank.com/",
        "image": "https://debank.com/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Wallet", "Dashboard", "Portfolio"]
        ]
    },
    {
        "name": "Furucombo",
        "url": "https://furucombo.app/",
        "image": "https://furucombo.app/favicon-512x512.png",
        "isDefi": true,
        "taxonomy": [
            ["Transaction", "Builder"]
        ]
    },
    {
        "name": "DeFi Saver",
        "url": "https://defisaver.com/",
        "image": "https://defisaver.com/favicon-32x32.png?v=c5674ddf3bb7dc7843705a5212f74941",
        "isDefi": true,
        "taxonomy": [
            ["Transaction", "Pre-built"],
            ["Investment Strategy", "Pre-built Strategy"]
        ]
    },
    {
        "name": "Kollateral",
        "url": "https://www.kollateral.co/",
        "image": "https://www.kollateral.co/icons-52fba9783a16f3a46510cb53ed7c2b2b/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Transaction", "Builder"]
        ]
    },
    {
        "name": "TxStreet",
        "url": "https://txstreet.com/",
        "image": "https://txstreet.com/static/img/icons/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Transaction", "Visualizer"]
        ]
    },
    {
        "name": "Fees.wtf",
        "url": "https://fees.wtf/",
        "image": "https://fees.wtf/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Wallet", "Dashboard", "Others"]
        ]
    },
    {
        "name": "Approved.zone",
        "url": "https://approved.zone/",
        "image": "https://approved.zone/assets/favicon/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Wallet", "Dashboard", "Others"]
        ]
    },
    {
        "name": "Claimable",
        "url": "https://claimable.vercel.app/",
        "image": "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/apple/271/helicopter_1f681.png",
        "isDefi": false,
        "taxonomy": [
            ["Wallet", "Dashboard", "Others"]
        ]
    },
    {
        "name": "Yieldfarming Info Contract Diff Checker",
        "url": "https://yieldfarming.info/tools/diff/",
        "image": "https://yieldfarming.info/favicon.svg",
        "isDefi": false,
        "taxonomy": [
            ["Developer", "Tool"]
        ]
    },
    {
        "name": "OpenZeppelin ERC20 Verifier",
        "url": "https://erc20-verifier.openzeppelin.com/",
        "image": "https://erc20-verifier.openzeppelin.com/favicon.ico",
        "isDefi": false,
        "taxonomy": [
            ["Developer", "Test"]
        ]
    },
    {
        "name": "Tenderly",
        "url": "https://tenderly.co/",
        "image": "https://tenderly.co/favicon-32x32.png?v=92da67d91ca0994a295e131f652d8697",
        "isDefi": false,
        "taxonomy": [
            ["Developer", "Test"],
            ["Developer", "Monitor"]
        ]
    },
    {
        "name": "Aave",
        "url": "https://aave.com/",
        "image": "https://aave.com/favicon32.ico",
        "isDefi": true,
        "taxonomy": [
            ["Credit", "Loan"],
            ["Credit", "Borrow"],
            ["Credit", "Flash Loan"]
        ]
    },
    {
        "name": "RealT",
        "url": "https://realt.co/",
        "image": "https://cdn1.realt.co/wp-content/uploads/2019/01/cropped-RealToken_Favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Tokenization", "Real Estate"]
        ]
    },
    {
        "name": "My Sardines",
        "url": "https://en.mysardines.com/",
        "image": "https://static.wixstatic.com/media/a3483d_a9f5467e6f8447f99b01d831eb1de852~mv2_d_2423_2121_s_2.png/v1/fill/w_32%2Ch_32%2Clg_1%2Cusm_0.66_1.00_0.01/a3483d_a9f5467e6f8447f99b01d831eb1de852~mv2_d_2423_2121_s_2.png",
        "isDefi": false,
        "taxonomy": [
            ["Tokenization", "Consumable"]
        ]
    },
    {
        "name": "NFTX",
        "url": "https://www.nftstandards.wtf/",
        "image": "https://publish.obsidian.md/favicon.ico?806c195b91fe002387c7",
        "isDefi": false,
        "taxonomy": [
            ["NFT", "Documentation"],
            ["Documentation", "NFT"],
        ]
    },
    {
        "name": "NFT Standards Wiki",
        "url": "https://nftx.io/",
        "image": "https://nftx.io/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["NFT", "Liquidity protocol"]
        ]
    },
    {
        "name": "NFTfi",
        "url": "https://nftfi.com/",
        "image": "https://nftfi.com/images/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["NFT", "Collaterized loan"],
            ["Credit", "Borrow"]
        ]
    },
    {
        "name": "ARCx Money",
        "url": "https://arcx.money/passport",
        "image": "https://arcx.money/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Identity"]
        ]
    },
    {
        "name": "Agora City",
        "url": "https://www.agora.city/",
        "image": "https://www.agora.city/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["Identity"],
            ["Media", "Public Figures"]
        ]
    },
    {
        "name": "Proof of Humanity",
        "url": "https://www.proofofhumanity.id/",
        "image": "https://uploads-ssl.webflow.com/5fd2301a2f3de083f679382b/5fd232a461642a025fb33740_favicon.png",
        "isDefi": true,
        "taxonomy": [
            ["Identity"]
        ]
    },
    {
        "name": "Ethereum Name Service",
        "url": "https://ens.domains/",
        "image": "https://ens.domains/static/favicon-6305d6ce89910df001b94e8a31eb08f5.ico",
        "isDefi": true,
        "taxonomy": [
            ["Identity"]
        ]
    },
    {
        "name": "NFT20",
        "url": "https://nft20.io/",
        "image": "https://nft20.io/favicon/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["NFT", "Liquidity protocol"]
        ]
    },
    {
        "name": "Genie",
        "url": "https://www.genie.xyz/",
        "image": "https://www.genie.xyz/geniefavicon.png",
        "isDefi": true,
        "taxonomy": [
            ["NFT", "Exchange aggregator"],
            ["Exchange", "DEX Aggregator"]
        ]
    },
    {
        "name": "Sorare",
        "url": "https://sorare.com/",
        "image": "https://sorare.com/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["NFT", "Gaming"]
        ]
    },
    {
        "name": "Axie Infinity",
        "url": "https://axieinfinity.com/",
        "image": "https://axieinfinity.com/favicon.png",
        "isDefi": true,
        "taxonomy": [
            ["NFT", "Gaming"]
        ]
    },
    {
        "name": "Luchadores.io",
        "url": "https://luchadores.io/",
        "image": "https://luchadores.io/favicon.png",
        "isDefi": true,
        "taxonomy": [
            ["NFT", "Gaming"]
        ]
    },
    {
        "name": "Cometh $MUST",
        "url": "https://www.cometh.io/",
        "image": "https://www.cometh.io/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["NFT", "Gaming"]
        ]
    },
    {
        "name": "NBA Top Shot",
        "url": "https://www.nbatopshot.com/",
        "image": "https://www.nbatopshot.com/static/favicon/favicon-512.png",
        "isDefi": true,
        "taxonomy": [
            ["NFT", "Gaming"]
        ]
    },
    {
        "name": "OpenSea",
        "url": "https://opensea.io/",
        "image": "https://opensea.io/static/images/logos/opensea-logo.png",
        "isDefi": false,
        "taxonomy": [
            ["NFT", "Exchange"]
        ]
    },
    {
        "name": "LooksRare ",
        "url": "https://looksrare.org/",
        "image": "https://looksrare.org/favicon-32x32.png",
        "isDefi": true,
        "taxonomy": [
            ["NFT", "Exchange"]
        ]
    },
    {
        "name": "Nexus Mutual",
        "url": "https://nexusmutual.io/",
        "image": "https://nexusmutual.io/assets/img/nxm-favicon.gif",
        "isDefi": true,
        "taxonomy": [
            ["Insurance"]
        ]
    },
    {
        "name": "Unslashed Finance",
        "url": "https://www.unslashed.finance/",
        "image": "https://www.unslashed.finance/favicon.ico",
        "isDefi": true,
        "taxonomy": [
            ["Insurance"]
        ]
    }
]