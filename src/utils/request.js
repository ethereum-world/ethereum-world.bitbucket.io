
export async function getForeignRequest(url, callback, catchBadResponse, catchError) {
    fetch(url, {
        method: "GET",
        mode: 'cors',
    }).then(response => {
        if (response.status === 200) {
            return response.json();
        } else {
            if (catchBadResponse != null)
                catchBadResponse(response);
        }
    }).then(jsonBody => {
        if (typeof jsonBody !== "undefined")
            callback(jsonBody);
    }).catch(error => {
        catchError(error);
    })
}